var indexSectionsWithContent =
{
  0: "abcdghijmprstwxyz",
  1: "abcghmsz",
  2: "xz",
  3: "abcghimrstwz",
  4: "acdhjmprswxyz",
  5: "r"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "functions",
  4: "properties",
  5: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Functions",
  4: "Properties",
  5: "Pages"
};

