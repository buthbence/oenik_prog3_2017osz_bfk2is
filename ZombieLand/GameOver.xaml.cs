﻿// <copyright file="GameOver.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ZombieLand
{
    using System;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// Interaction logic for GameOver.xaml
    /// </summary>
    public partial class GameOver : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GameOver"/> class
        /// </summary>
        public GameOver()
        {
            this.Background = new ImageBrush(new BitmapImage(new Uri(@"Resources\jatekvege.jpg", UriKind.Relative)));
            this.InitializeComponent();
            this.KeyDown += this.Window_keydown;
        }

        private void Window_keydown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.R)
            {
                Game game = new Game();
                this.Close();
                game.ShowDialog();
            }

            if (e.Key == Key.Escape)
            {
                MainWindow menu = new MainWindow();
                this.Close();
                menu.ShowDialog();
            }
        }
    }
}
