﻿// <copyright file="MainWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ZombieLand
{
    using System;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class
        /// </summary>
        public MainWindow()
        {
            this.Background = new ImageBrush(new BitmapImage(new Uri(@"Resources\kezdokepernyo.jpg", UriKind.Relative)));
            this.InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Game game = new Game();
            this.Close();
            game.ShowDialog();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            HighScores highscores = new HighScores();
            highscores.ShowDialog();
        }
    }
}
