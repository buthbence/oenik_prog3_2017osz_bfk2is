﻿// <copyright file="AmmoPack.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ZombieLand
{
    using System;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// class for Ammopack
    /// </summary>
    public class AmmoPack : GameItem
    {
        private GeometryGroup ammoPackShape;
        private ImageBrush ammo;
        private int ammoPackSizeValue = 5;

        /// <summary>
        /// Initializes a new instance of the <see cref="AmmoPack"/> class
        /// </summary>
        /// <param name="centerX">Horizontal location of ammopack</param>
        /// <param name="centerY">Vertical location of ammopack</param>
        public AmmoPack(double centerX, double centerY)
            : base(centerX, centerY)
        {
            this.Make_AmmoPack();
        }

        /// <summary>
        /// Gets gets or sets a value indicating whether it's size of ammopack
        /// </summary>
        public int AmmoPack_Size
        {
            get
            {
                return this.ammoPackSizeValue;
            }
        }

        /// <summary>
        /// Gets gets or sets a value indicating whether it's a picture of ammo.
        /// </summary>
        public ImageBrush Ammo
        {
            get
            {
                return this.ammo;
            }
        }

        /// <summary>
        /// Make an ammopack object.
        /// </summary>
        public void Make_AmmoPack()
        {
            this.Width = 60;
            this.Height = 30;
            this.ammo = new ImageBrush(new BitmapImage(new Uri(@"Resources\ammo.png", UriKind.Relative)));
            this.ammoPackShape = new GeometryGroup();
            this.ammoPackShape.Children.Add(new RectangleGeometry(new Rect(0, 0, this.Width, this.Height)));
            this.Area = this.ammoPackShape.GetFlattenedPathGeometry();
        }
    }
}
