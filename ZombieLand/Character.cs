﻿// <copyright file="Character.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ZombieLand
{
    using System;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// Class of the characters
    /// </summary>
    public class Character : GameItem
    {
        private double speed;
        private bool dead;

        /// <summary>
        /// Initializes a new instance of the <see cref="Character"/> class
        /// </summary>
        /// <param name="centerX">Horizontal location of the character</param>
        /// <param name="centerY">Vertical location of the character</param>
        public Character(double centerX, double centerY)
    : base(centerX, centerY)
        {
            this.Height = 40;
            this.Width = 40;
            this.Area = new RectangleGeometry(new Rect(0, 0, this.Width, this.Height)).GetFlattenedPathGeometry();
        }

        /// <summary>
        /// Gets or sets a value indicating the value of the horizontal moving
        /// </summary>
        public double Xcoord { get; set; }

        /// <summary>
        /// Gets or sets a value indicating the value of the vertical moving
        /// </summary>
        public double Ycoord { get; set; }

        /// <summary>
        /// Gets or sets a value indicating character health
        /// </summary>
        public double Health { get; set; }

        /// <summary>
        /// Gets or sets a value indicating value of the character speed
        /// </summary>
        public double Speed
        {
            get
            {
                return this.speed;
            }

            set
            {
                this.speed = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the character is dead or not.
        /// </summary>
        public bool Dead
        {
            get
            {
                return this.dead;
            }

            set
            {
                this.dead = value;
            }
        }

        /// <summary>
        /// Method of the character turning.
        /// </summary>
        /// <param name="point">Location of the mouse.</param>
        public void Turn(Point point)
        {
            this.Radian = Math.Atan2(point.Y - this.CenterY, point.X - this.CenterX);
        }

        /// <summary>
        /// Moving of the character.
        /// </summary>
        public void Ticking()
        {
            this.CenterX += this.Xcoord;
            this.Xcoord = 0;
            this.CenterY += this.Ycoord;
            this.Ycoord = 0;
        }
    }
}
