﻿// <copyright file="Hero.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ZombieLand
{
    using System;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// Class of the hero
    /// </summary>
    public class Hero : Character
    {
        private int ammo;
        private ImageBrush jatekos;

        /// <summary>
        /// Initializes a new instance of the <see cref="Hero"/> class
        /// </summary>
        /// <param name="centerX">Horizontal location of the hero.</param>
        /// <param name="centerY">Vertical location of the hero.</param>
        public Hero(double centerX, double centerY)
            : base(centerX, centerY)
        {
            this.jatekos = new ImageBrush(new BitmapImage(new Uri(@"Resources\jatekos_kicsi.png", UriKind.Relative)));
            this.Health = 100;
            this.Ammo = 5;
            this.Speed = 10;
            this.Dead = false;
        }

        /// <summary>
        /// Gets or sets a value indicating the ammo of the hero.
        /// </summary>
        public int Ammo
        {
            get
            {
                return this.ammo;
            }

            set
            {
                this.ammo = value;
            }
        }

        /// <summary>
        /// Gets gets or sets a value indicating Rotate transform of the hero picture.
        /// </summary>
        public ImageBrush Jatekos
        {
            get
            {
                this.jatekos.Transform = new RotateTransform(this.Degree, this.CenterX + (this.Width / 2), this.CenterY + (this.Height / 2));
                this.jatekos.Stretch = Stretch.None;
                return this.jatekos;
            }
        }

        /// <summary>
        /// Moving of the hero.
        /// </summary>
        public void Move()
        {
            if (Keyboard.IsKeyDown(Key.W))
            {
                this.Ycoord -= this.Speed;
            }

            if (Keyboard.IsKeyDown(Key.A))
            {
                this.Xcoord -= this.Speed;
            }

            if (Keyboard.IsKeyDown(Key.S))
            {
                this.Ycoord += this.Speed;
            }

            if (Keyboard.IsKeyDown(Key.D))
            {
                this.Xcoord += this.Speed;
            }

            this.Ticking();
           this.CoordinatesChange();
        }

        /// <summary>
        /// Hero methods collection
        /// </summary>
        /// <param name="point">Location of the mouse.</param>
        public void Tick(Point point)
        {
            this.Turn(point);
            this.Move();
            this.CoordinatesChange();
        }

        /// <summary>
        /// Hero stop
        /// </summary>
        public void Stop()
        {
            this.Ycoord = 0;
            this.Xcoord = 0;
        }

        /// <summary>
        /// If the hero collided with the wall does not improve his xcoord and ycoord.
        /// </summary>
        public void CoordinatesChange()
        {
            if (this.CenterX <= 0)
            {
                this.CenterX = 0;

               // Xcoord = Xcoord > 0 ? Xcoord : 0;
            }

            if (this.CenterY <= 0)
            {
                this.CenterY = 0;

                // Ycoord = Ycoord > 0 ? Ycoord : 0;
            }

            if (this.CenterX >= this.MapWidth - this.Width)
            {
                this.CenterX = this.MapWidth - this.Width;

                // Xcoord = Xcoord > MapWidth ? Xcoord : 0;
            }

            if (this.CenterY >= this.MapHeight - this.Height)
            {
                this.CenterY = this.MapHeight - this.Height;

               // Ycoord = Ycoord > MapHeight ? Ycoord : 0;
            }
        }

        /// <summary>
        /// Hero is dead or not.
        /// </summary>
        /// <returns>Whether hero is dead or not in bool parameter</returns>
        public bool IsDead()
        {
            if (this.Health <= 0)
            {
                this.Dead = true;
            }

            return this.Dead;
        }
    }
}
