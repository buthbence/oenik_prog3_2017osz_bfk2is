﻿// <copyright file="HighScores.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ZombieLand
{
    using System.Windows;

    /// <summary>
    /// Interaction logic for HighScores.xaml
    /// </summary>
    public partial class HighScores : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="HighScores"/> class
        /// </summary>
        public HighScores()
        {
            this.InitializeComponent();
            this.Score = new Score();
            this.Score.Sort();
            this.DataContext = this.Score;
        }

        /// <summary>
        /// Gets or sets a value indicating the Score
        /// </summary>
        public Score Score { get; set; }
    }
}
