﻿// <copyright file="Model.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ZombieLand
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Class of the Viewmodel.
    /// </summary>
    internal class Model
    {
        private Random r;
        private int zombieCount;

        /// <summary>
        /// Initializes a new instance of the <see cref="Model"/> class
        /// </summary>
        public Model()
        {
            this.r = new Random();
            this.zombieCount = 5;
            this.Bullets = new List<Bullet>();
            this.Zombies = new List<Zombie>();
            this.Hero = new Hero(Screen.Mheight / 2, Screen.Mwidth / 2);
            this.Healthbar = new HealthBar(Screen.Mwidth - 150, 20, this.Hero);
            this.Zombies_Fill(this.Zombies);
        }

        /// <summary>
        /// Gets or sets a value indicating the hero.
        /// </summary>
        public Hero Hero { get; set; }

        /// <summary>
        /// Gets or sets a value indicating the Zombie.
        /// </summary>
        public Zombie Zombie { get; set; }

        /// <summary>
        /// Gets or sets a value indicating the list of bullets.
        /// </summary>
        public List<Bullet> Bullets { get; set; }

        /// <summary>
        /// Gets or sets a value indicating the ammopack
        /// </summary>
        public AmmoPack AmmoPack { get; set; }

        /// <summary>
        /// Gets or sets a value indicating the list of zombies.
        /// </summary>
        public List<Zombie> Zombies { get; set; }

        /// <summary>
        /// Gets or sets a value indicating the hero's healthbar
        /// </summary>
        public HealthBar Healthbar { get; set; }

        /// <summary>
        /// Upload the zombie list with zombie objects.
        /// </summary>
        /// <param name="zombies">Which list will be uploaded.</param>
        public void Zombies_Fill(List<Zombie> zombies)
        {
            int i = 0;
            while (i < this.zombieCount)
            {
                zombies.Add(new Zombie(this.r.Next(50, 750), this.r.Next(50, 550)));
                i++;
            }
        }

        /// <summary>
        /// See the zombie collisons.
        /// </summary>
        public void Zombie_Collided()
        {
            for (int i = 0; i < this.Zombies.Count - 1; i++)
            {
                for (int j = 0; j < this.Zombies.Count; j++)
                {
                    if (this.Zombies.ElementAt(i).Position.Bounds.IntersectsWith(this.Zombies.ElementAt(j).Position.Bounds) && i != j && i < j)
                    {
                        this.Zombies.ElementAt(j).CenterX = this.Zombies.ElementAt(j).CenterX + 1;
                        this.Zombies.ElementAt(j).CenterY = this.Zombies.ElementAt(j).CenterY + 1;
                    }
                }
            }
        }

        /// <summary>
        /// Drop down the ammopack.
        /// </summary>
        public void Drop_Ammo()
        {
            this.AmmoPack = new AmmoPack(this.r.Next(100, 700), this.r.Next(100, 500));
        }

        /// <summary>
        /// Ammopack pick up by the hero.
        /// </summary>
        public void PickUp()
        {
            if (this.AmmoPack.Position.Bounds.IntersectsWith(this.Hero.Position.Bounds))
            {
                this.Hero.Ammo += this.AmmoPack.AmmoPack_Size;
                this.AmmoPack = null;
            }
        }
    }
}
