﻿// <copyright file="GameItem.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ZombieLand
{
    using System;
    using System.Windows.Media;

    /// <summary>
    /// class of gameitem
    /// </summary>
    public class GameItem
    {
        private Geometry area;
        private double degree;
        private double width;
        private double height;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameItem"/> class
        /// </summary>
        /// <param name="centerX">Horizontal Location of a character</param>
        /// <param name="centerY">Vertical location of a character</param>
        public GameItem(double centerX, double centerY)
        {
            this.CenterX = centerX;
            this.CenterY = centerY;
            this.MapHeight = Screen.Mheight;
            this.MapWidth = Screen.Mwidth;
        }

        /// <summary>
        /// Gets or sets a value indicating width of a character
        /// </summary>
        public double Width
        {
            get
            {
                return this.width;
            }

            set
            {
                this.width = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating height of a character
        /// </summary>
        public double Height
        {
            get
            {
                return this.height;
            }

            set
            {
                this.height = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating rotation of a character
        /// </summary>
        public double Radian
        {
            get
            {
                return Math.PI * this.degree / 180;
            }

            set
            {
                this.degree = 180 * value / Math.PI;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating horizontal location of a character
        /// </summary>
        public double CenterX { get; set; }

        /// <summary>
        /// Gets or sets a value indicating vertical location of a character
        /// </summary>
        public double CenterY { get; set; }

        /// <summary>
        /// Gets or sets a value indicating height of the window
        /// </summary>
        public double MapHeight { get; set; }

        /// <summary>
        /// Gets or sets a value indicating width of the window
        /// </summary>
        public double MapWidth { get; set; }

        /// <summary>
        /// Gets gets or sets a value indicating position of the characters
        /// </summary>
        public Geometry Position
        {
            get
            {
                TransformGroup tg = new TransformGroup();
                tg.Children.Add(new TranslateTransform(this.CenterX, this.CenterY));
                tg.Children.Add(new RotateTransform(this.Degree, this.CenterX + (this.width / 2), this.CenterY + (this.height / 2)));
                this.area.Transform = tg;
                return this.area.GetFlattenedPathGeometry();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating area of the characters
        /// </summary>
        public Geometry Area
        {
            get
            {
                return this.area;
            }

            set
            {
                this.area = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating the amount of rotate in double.
        /// </summary>
        public double Degree
        {
            get
            {
                return this.degree;
            }

            set
            {
                this.degree = value;
            }
        }
    }
}
