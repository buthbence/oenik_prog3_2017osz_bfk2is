﻿// <copyright file="Screen.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ZombieLand
{
    using System;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Threading;

    /// <summary>
    /// Class of the Screen.
    /// </summary>
    internal class Screen : FrameworkElement
    {
        private DispatcherTimer timer;
        private Pen pen = new Pen(Brushes.Blue, 3);
        private Model model;

        /// <summary>
        /// Initializes a new instance of the <see cref="Screen"/> class
        /// </summary>
        public Screen()
        {
            this.Loaded += this.Screen_Loading;
        }

        /// <summary>
        /// Gets or sets a value indicating the map height
        /// </summary>
        public static double Mheight { get; set; }

        /// <summary>
        /// Gets or sets a value indicating the map width
        /// </summary>
        public static double Mwidth { get; set; }

        /// <summary>
        /// Screen loading
        /// </summary>
        /// <param name="sender">Sender control</param>
        /// <param name="e">Event arguments</param>
        public void Screen_Loading(object sender, RoutedEventArgs e)
        {
            Mheight = this.ActualHeight;
            Mwidth = this.ActualWidth;
            Game window = this.Parent as Game;
            this.model = new Model();
            if (window != null)
            {
                this.timer = new DispatcherTimer();
                this.timer.Interval = TimeSpan.FromMilliseconds(10);
                this.timer.Tick += this.Timer_Ticking;
                window.KeyUp += this.Window_KeyUp;
                window.KeyDown += this.Window_Space;
                this.InvalidateVisual();
                this.timer.Start();
            }
        }

        /// <summary>
        /// Space keydown to shoot.
        /// </summary>
        /// <param name="sender">Sender control</param>
        /// <param name="e">Event arguments</param>
        public void Window_Space(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                if (this.model.Hero.Ammo >= 0)
                {
                    this.model.Bullets.Add(new Bullet(
                    this.model.Hero.CenterX + (this.model.Hero.Width / 2) + Math.Cos(this.model.Hero.Radian), this.model.Hero.CenterY + (this.model.Hero.Height / 2) + Math.Sin(this.model.Hero.Radian), this.model.Hero.Radian)); // hova tegye a bulletet.
                    this.model.Hero.Ammo--;
                }

                if (this.model.Hero.Ammo == 1)
                {
                    this.model.Drop_Ammo();
                }
            }
        }

        /// <summary>
        /// Stop the hero moving if the key is up.
        /// </summary>
        /// <param name="sender">Sender control</param>
        /// <param name="e">Event arguments</param>
        public void Window_KeyUp(object sender, KeyEventArgs e) // window.keydown miatt kell object,keyeventargs
        {
            this.model.Hero.Stop();
        }

        /// <summary>
        /// Timer tick
        /// </summary>
        /// <param name="sender">Sender control</param>
        /// <param name="e">Event arguments</param>
        public void Timer_Ticking(object sender, EventArgs e) // timer.tick miatt kell object,eventargs
        {
            this.model.Hero.Tick(Mouse.GetPosition(this));
            this.model.Bullets.RemoveAll(bullet => bullet.Tick(this.model.Zombies));
            if (this.model.AmmoPack != null)
            {
                this.model.PickUp();
            }

            if (this.model.Hero.IsDead() == true)
            {
                this.timer.Stop();
                HighScores highscore = new HighScores();
                highscore.Score.WriteTxt();
                highscore.Score.Sort();
                highscore.Close();
                Zombie.Count = 0;
                Game window = this.Parent as Game;
                GameOver gameover = new GameOver();
                window.Close();
                gameover.ShowDialog();
            }

            this.model.Zombies.ForEach(zombie => zombie.Tick(this.model.Hero, new Point(this.model.Hero.Position.Bounds.Location.X, this.model.Hero.Position.Bounds.Location.Y), this.model.Healthbar));
            this.model.Zombie_Collided();
            this.model.Zombies.RemoveAll(zombie => zombie.IsDead());
            this.model.Healthbar.Make_HealthBar(this.model.Hero);
            this.InvalidateVisual();
        }

        /// <summary>
        /// Editing
        /// </summary>
        /// <param name="drawingContext">drawing editing</param>
        protected override void OnRender(DrawingContext drawingContext)
        {
            if (this.model != null)
            {
                drawingContext.DrawGeometry(this.model.Hero.Jatekos, null, this.model.Hero.Position);
                this.model.Zombies.ForEach(zombie => drawingContext.DrawGeometry(zombie.Zombi, null, zombie.Position));
                this.model.Bullets.ForEach(bullet => drawingContext.DrawGeometry(Brushes.Red, this.pen, bullet.Position));
                drawingContext.DrawGeometry(Brushes.Red, this.pen, this.model.Healthbar.Position);
                if (this.model.AmmoPack != null)
                {
                    drawingContext.DrawGeometry(this.model.AmmoPack.Ammo, null, this.model.AmmoPack.Position);
                }
            }
        }
    }
}
