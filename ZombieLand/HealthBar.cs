﻿// <copyright file="HealthBar.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ZombieLand
{
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// Class of Healthbar
    /// </summary>
    public class HealthBar : GameItem
    {
        private RectangleGeometry rect;

        /// <summary>
        /// Initializes a new instance of the <see cref="HealthBar"/> class
        /// </summary>
        /// <param name="centerX">Horizontal location of the healthbar</param>
        /// <param name="centerY">Vertical location of the healthbar</param>
        /// <param name="hero">Healthbar of the current hero.</param>
        public HealthBar(double centerX, double centerY, Hero hero)
            : base(centerX, centerY)
        {
            this.Make_HealthBar(hero);
        }

        /// <summary>
        /// Method of the healthbar creation
        /// </summary>
        /// <param name="hero">Healthbar of the hero.</param>
        public void Make_HealthBar(Character hero)
        {
            this.Width = hero.Health;
            this.Height = 30;
            if (this.Width >= 0)
            {
                this.rect = new RectangleGeometry(new Rect(0, 0, this.Width, this.Height));
                this.Area = this.rect.GetFlattenedPathGeometry();
            }
        }
    }
}
