﻿// <copyright file="Score.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ZombieLand
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    /// <summary>
    /// Class ot he highscore
    /// </summary>
    public class Score
    {
        private List<string> highscores;
        private string[] s = new string[10];
        private string text = (Zombie.Count / 2).ToString();

        /// <summary>
        /// Initializes a new instance of the <see cref="Score"/> class
        /// </summary>
        public Score()
        {
            this.highscores = new List<string>();
            this.ReadTxt();
        }

        /// <summary>
        /// Gets or sets a value indicating list of the highscore
        /// </summary>
        public List<string> Highscores
        {
            get
            {
                return this.highscores;
            }

            set
            {
                this.highscores = value;
            }
        }

        /// <summary>
        /// Write the new result into the txt.
        /// </summary>
        public void WriteTxt()
        {
            int result = this.s.Count(s => s != null);
            if (result == 10)
            {
                result = 9;
            }

            for (int i = 0; i <= result; i++)
            {
                if (this.s.Length > result + 1)
                {
                    this.s[result] = this.text;
                    break;
                }

                if (this.s[i] != null && (Zombie.Count / 2) >= int.Parse(this.s[i]))
                {
                    this.s[9] = this.text;
                    break;
                }
            }

            int resultwrite = this.s.Count(s => s != null);
            using (StreamWriter sw = new StreamWriter(@"Resources\highscores.txt"))
            {
                for (int i = 0; i < resultwrite; i++)
                {
                    sw.WriteLine(this.s[i]);
                }

                sw.Close();
            }
        }

        /// <summary>
        /// Sort the arraz of the highscores.
        /// </summary>
        public void Sort()
        {
            string save;
            int result = this.s.Count(s => s != null);
            for (int i = 0; i < result - 1; i++)
            {
                for (int j = i + 1; j < result; j++)
                {
                    if (this.s[j] != null && this.s[i] != null)
                    {
                        if (int.Parse(this.s[i]) < int.Parse(this.s[j]))
                        {
                            save = this.s[i];
                            this.s[i] = this.s[j];
                            this.s[j] = save;
                        }
                    }
                }
            }

            foreach (string item in this.s)
            {
                this.highscores.Add(item);
            }
        }

        /// <summary>
        /// Read the sorted array into the txt file.
        /// </summary>
        public void ReadTxt()
        {
            using (StreamReader r = new StreamReader(@"Resources\highscores.txt"))
            {
                for (int i = 0; i < this.s.Length; i++)
                {
                    this.s[i] = r.ReadLine();
                }

                r.Close();
            }
        }
}
}
