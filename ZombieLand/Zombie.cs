﻿// <copyright file="Zombie.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ZombieLand
{
    using System;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// Class of zombie
    /// </summary>
    public class Zombie : Character
    {
        private int attack;
        private ImageBrush zombi;

        /// <summary>
        /// Initializes a new instance of the <see cref="Zombie"/> class
        /// </summary>
        /// <param name="centerX">Horizontal Location of the zombie.</param>
        /// <param name="centerY">Vertical location of the zombie.</param>
        public Zombie(double centerX, double centerY)
            : base(centerX, centerY)
        {
            this.zombi = new ImageBrush(new BitmapImage(new Uri(@"Resources\zombi2_kicsi.png", UriKind.Relative)));
            this.Health = 1;
            this.attack = 1;
            this.Speed = 1;
            this.Dead = false;
        }

        /// <summary>
        /// Gets or sets a value indicating how many zombies died in the game.
        /// </summary>
        public static int Count { get; set; }

        /// <summary>
        /// Gets gets or sets a value indicating Rotate transform of the zombi picture.
        /// </summary>
        public ImageBrush Zombi
        {
            get
            {
                this.zombi.Transform = new RotateTransform(this.Degree, this.CenterX + (this.Width / 2), this.CenterY + (this.Height / 2));
                this.zombi.Stretch = Stretch.None;
                return this.zombi;
            }
        }

        /// <summary>
        /// Zombie moving method
        /// </summary>
        /// <param name="target">target to follow</param>
        public void Move(GameItem target)
        {
            if (target.Position.Bounds.Location.X > this.Position.Bounds.Location.X)
            {
                this.Xcoord += this.Speed;
            }

            if (target.Position.Bounds.Location.X < this.Position.Bounds.Location.X)
            {
                this.Xcoord -= this.Speed;
            }

            if (target.Position.Bounds.Location.Y > this.Position.Bounds.Location.Y)
            {
                this.Ycoord += this.Speed;
            }

            if (target.Position.Bounds.Location.Y < this.Position.Bounds.Location.Y)
            {
                this.Ycoord -= this.Speed;
            }

            this.Ticking();
        }

        /// <summary>
        /// Method of zombie attack
        /// </summary>
        /// <param name="target">Target to attack.</param>
        /// <param name="bar">Healthbar of the hero.</param>
        public void Attack(Character target, GameItem bar)
        {
            if (target.Position.Bounds.IntersectsWith(this.Position.Bounds) && target.Dead == false)
            {
                target.Health -= this.attack;
                bar.Width = target.Health;
            }
        }

        /// <summary>
        /// Collection of the zombie method
        /// </summary>
        /// <param name="target">Target to attack,follow</param>
        /// <param name="point">Hero location</param>
        /// <param name="bar">Hero heathbar</param>
        public void Tick(Character target, Point point, GameItem bar)
        {
            this.Move(target);
            this.Attack(target, bar);
            this.Turn(point);
        }

        /// <summary>
        /// Zombie dead or not
        /// </summary>
        /// <returns>Whether zombie dead or not</returns>
        public bool IsDead()
        {
            if (this.Health <= 0)
            {
                Count++;
                this.Dead = true;
            }

            return this.Dead;
        }
    }
}
