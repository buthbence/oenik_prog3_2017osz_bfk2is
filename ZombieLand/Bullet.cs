﻿// <copyright file="Bullet.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ZombieLand
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// Class of bullet
    /// </summary>
    public class Bullet : GameItem
    {
        private double speed;
        private int damage;
        private bool destroy = false;
        private Random randomPos = new Random();

        private GeometryGroup bulletshape;

        /// <summary>
        /// Initializes a new instance of the <see cref="Bullet"/> class
        /// </summary>
        /// <param name="centerX">Horizontal location</param>
        /// <param name="centerY">Vertical location</param>
        /// <param name="radian">Degree of Hero</param>
        public Bullet(double centerX, double centerY, double radian)
            : base(centerX, centerY)
        {
            this.Make_Bullet();
            this.Xcoord = Math.Cos(radian) * this.speed;
            this.Ycoord = Math.Sin(radian) * this.speed;
        }

        /// <summary>
        /// Gets or sets a value indicating it's horizontal location of bullet.
        /// </summary>
        public double Xcoord { get; set; }

        /// <summary>
        /// Gets or sets a value indicating  it is vertical location of bullet.
        /// </summary>
        public double Ycoord { get; set; }

        /// <summary>
        /// Gets or sets a value indicating the damage of the bullet.
        /// </summary>
        public int Damage
        {
            get
            {
                return this.damage;
            }

            set
            {
                this.damage = value;
            }
        }

        /// <summary>
        /// Method of creating bullet
        /// </summary>
        public void Make_Bullet()
        {
            this.Height = 3;
            this.Width = 3;
            this.speed = 20;
            this.damage = 1;
            this.bulletshape = new GeometryGroup();
            this.bulletshape.Children.Add(new RectangleGeometry(new Rect(0, 0, this.Height, this.Width)));
            this.Area = this.bulletshape.GetFlattenedPathGeometry();
        }

        /// <summary>
        /// Method of the bullet move.
        /// </summary>
        /// <returns>Returns which bullet have to be destroyed.</returns>
        public bool Move()
        {
                this.CenterX += this.Xcoord;
                this.CenterY += this.Ycoord;
            if (this.CenterX - this.Width < 0 || this.CenterX > this.MapWidth || this.CenterY + this.Height < 0 || this.CenterY > this.MapHeight)
            {
                this.destroy = true;
            }

            return this.destroy;
        }

        /// <summary>
        /// Method of bullet attacking if its intersect with a zombie.
        /// </summary>
        /// <param name="zombies">List of zombies</param>
        /// <returns>If intersect destroy the zombie element</returns>
        public bool Attack(List<Zombie> zombies)
        {
            for (int i = 0; i < zombies.Count; i++)
            {
                if (zombies.ElementAt(i).Position.Bounds.IntersectsWith(this.Position.Bounds) && zombies.ElementAt(i).Dead == false)
                {
                    zombies.ElementAt(i).Health -= this.damage;
                    if (zombies.ElementAt(i).IsDead() == true)
                    {
                        zombies.Add(new Zombie(this.randomPos.Next(50, 550), this.randomPos.Next(50, 750)));
                    }

                    this.destroy = true;
                }
            }

            return this.destroy;
        }

        /// <summary>
        /// Collection of bullet methods
        /// </summary>
        /// <param name="zombies">List of zombies.</param>
        /// <returns>Returns the destroyable objects.</returns>
        public bool Tick(List<Zombie> zombies)
        {
            this.Move();
            this.Attack(zombies);
            return this.destroy;
        }
    }
}
